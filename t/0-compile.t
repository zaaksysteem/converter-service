# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

use Test::Compile;

# This module has an "INIT" block.
# If it isn't loaded before runtime starts, it'll output a warning once it
# gets loaded.
use Glib::Object::Introspection;

my $test = Test::Compile->new();
$test->all_files_ok();
$test->done_testing();